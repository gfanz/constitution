# Constitution

This repository is for a latex formatted version of the GFANZ constitution. We will use it to propose and track changes to the constitution.

There are 7 files here:
GFANZ Constitution Original.pdf 
GFANZ Constitution Original.tex
GFANZ Constitution 2019.pdf
GFANZ Constitution 2019.tex
GFANZ Constitution Proposed 2021.pdf
GFANZ Constitution Proposed 2021.tex
README.md 


The pdf versions are meant to be nicely formatted and easy to read. The tex versions are the source code for the pdf versions. The changes between these can be viewed here:https://gitlab.com/gfanz/constitution/-/commit/274b3cd057b0d2787a0f2bf4e92aa16341aa61cf?view=parallel&w=1


# Proposal to amend the constitution

I, Rob Elshire (President GFANZ), propose to ammend the constitution from the 'GFANZ Constitution 2019.pdf' to 'GFANZ Constitution Proposed 2021.pdf'. The changes are for the purpose  1) Change 'The Treaty of Waitangi' to 'Te Tiriti o Waitangi'and 2) Add two council positions, bringing the total to 5 councilors.

A correction for the registered address for GFANZ was also made.

A vote will be made at the AGM on the 5th of November 2021. If these changes pass the vote, they will become effective at that time.
